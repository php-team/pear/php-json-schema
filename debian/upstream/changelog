5.3.0 - Jul 6, 2024

    Bumped minimum PHP requirement to 7.1.0 #731
    Fixed PHP 8.4 deprecation warnings #731

5.2.13 - Sep 26, 2023

    [Backport] Add attribute #[\AllowDynamicProperties] to allow applying defaults without deprecation warning (#695)

5.2.12 - Apr 13, 2022

    Backport Fix type validation failing for "any" and false-y type wording (#686)

5.2.11 - Jul 22, 2021

   #669 Backports PHP 8.1 support

5.2.10 - May 27, 2020

   #631 Backports for 5.2.10: Fixes in build process and PHP 8

5.2.9 - Sep 25, 2019

   #591 Backports for 5.2.9

5.2.8 - Jan 15, 2019

   #558 Backports for 5.2.8

5.2.7 - Feb 14, 2018

   #495 Backports for 5.2.7

5.2.6 - Oct 21, 2017

   #460 Backports for 5.2.6

5.2.5 - Oct 10, 2017

   Backports for 5.2.5 #457

5.2.4 - Oct 4, 2017

   Fresh tag to rectify 5.2.3 mistag.

5.2.3 - Oct 4, 2017

   #453 Backports for 5.2.3

5.2.2 - Oct 2, 2017

   #431 Backports for 5.2.2 (Part 1)
   #432 Added property name in draft-3 required error
   #433 Backports for 5.2.2 (Part 2)
   #450 Backports for 5.2.2 (Part 3)

5.2.1 - May 16, 2017

    #353 Validation of JSON-Schema
    #405 fix bug when applying defaults
    #408 SchemaStorage::addSchema() should call BaseConsstraint::arrayToObjectRecursive() on the provide schemas
    #409 [BUGFIX] Cast empty schema arrays to object
    #411 [BUGFIX] Split $objectDefinition into $schema and $properties
    #415 Issue-414: Allow The Option of T or space for Date time.
    #416 Testcase for minProperties with properties defined + Fix Test
    #419 [BUGFIX] Split "uri" format into "uri" & "uri-reference", fix meta-schema bug
    #421 [BUGFIX] Tweak phpdocumentor dependency to avoid install conflicts

5.2.0 - Mar 22, 2017

    #362 (package:// URIs and bundle local copy of the spec schemas)
    #372 (bugfix for issue #371)
    #378 (bugfix for #376, add provided schema under a dummy URI)
    #382 (add quiet / verbose options to CLI script)
    #383 (add CHECK_MODE_DISABLE_FORMAT)
    #366 (more unit tests)
    #389 (allow assoc array as schema)
    #398 (enable FILTER_FLAG_EMAIL_UNICODE if available)
    #394 (anyOf / oneOf exceptions - bugfix for #393)
    #365 (default recursion & bugfixes for #359 & #377, refactor defaults, other minor fixes)
    #357 (schema validation)
    #400 (remove stale files from #357)

5.1.0 - Feb 21, 2017

    #349 Add option to apply default values from the schema
    #354 Option to throw exceptions when an error is encountered
    #355 Make code-style more consistent

5.0.0 - Feb 15, 2017

    #347 Fix regex for Rfc3339 validation
    #351 Add new config option, refactor coerce feature to use this, and simplify the public API

4.1.0 - Dec 22, 2016

    #335 Fix various details in README.md
    #336 Better coercion
    #339 Added SchemaStorageInterface
    #342 fix #341 validate-json command

4.0.1 - Nov 9, 2016

    #322 handle coercion of multiple types
    #325 handle negative integers
    #326 update readme for 4.0.0
    #328 add php lang tag to code block
    #330 [BUGFIX] Use unicode matching for patterns

4.0.0 - Oct 10, 2016

    #304 Return points, spelling and dead code
    #308 Add support for type coercion
    #310 performance improvements
    #314 Updated to check that $ref is a string
    #316 fix(NumberConstraint): force positive value on first arg
    #317 Add support for $ref on properties
    #318 chore(phpunit): ignore local phpunit.xml
    #319 feat(demo): add simple demo script

3.0.1 - Aug 26, 2016

    #306 Master is not 2.0.x anymore but 3.0.x

3.0.0 - Aug 15, 2016

    #234 Fix: allow explicit 0 secfracs in datetime format
    #276 use JsonPointer for path handling
    #277 New way of handling references
    #278 Exception interface
    #282 fix instancing wrong type, breaks inheritance
    #283 move MinMaxPropertiesTest to proper location
    #286 Support json_decode assoc
    #287 Updating to use more correct hostname regex. This fixes #129
    #290 Cache the results of RefResolver::resolve()
    #292 Fix #291 failed tests with lestest PHP
    #294 Change error reporting for invalid types with multiple valid types

2.0.5 - Jun 2, 2016

    #272 chore(composer): remove version in favor of VCS tags
    #273 Throw ResourceNotFoundException outside of re-defined error-handler
    #275 Fix constraint of error

2.0.4 - May 24, 2016

    #271 Revert "Do not override error handler"


2.0.3 - May 10, 2016

    #262 Do not override error handler


2.0.2 - May 9, 2016

    #261 Fixes min/max properties validation for non objects


2.0.1 - Apr 28, 2016

    #259 feat(UriRetriever): application/json support


2.0.0 - Apr 14, 2016

    #174 Enhancement: Require more recent phpunit/phpunit
    #223 Fix: Remove conditions never evaluating to true
    #227 Fix: Generate coverage on one PHP version only
    #231 fix(composer): reduce minimum php to 5.3.3
    #232 Moving constraints min- and maxProperties to ObjectConstraint
    #238 add rfc3339 helper class
    #245 #240 Fix RefResolver and make it compatible with draft-04
    #246 chore(LICENSE): switch to MIT


1.6.1 - Jan 25, 2016

    #213 Fix exclusive min/max strictness
    #217 fixup dev-master alias
    #218 feat(composer): require PHP 5.3.29 (PHP 5.3.x EOL version) minimum
    #221 feat(Factory): add setConstraintClass
    #222 feat(travis): pivot to container-based infrastructure


1.6.0 - Jan 6, 2016

    #142 Optional extra arguments for custom error messages
    #143 Add constraint factory
    #192 Create .gitattributes
    #194 bugfix: patternProperties raised errors when the pattern has slashes
    #202 Fix CollectionConstraint to allow uniqueItems to be false
    #204 Fix path output for required properties
    #206 An email is a string, not much else.
    #207 Fix non-6 digit microsecond date time formats
    #209 RefResolver::$depth restoration after JsonDecodingException


1.5.0 - Sep 8, 2015

    #182 Fix #93 ($ref to local definition not working)


1.4.4 - Jul 14, 2015

    #161 Throw exception if max depth exceeded
    #168 Enhancement: Speed up builds as much as possible
    #170 Fix: Start all messages with a capital letter
    #171 Fix: Be more specific with error message


1.4.3 - Jul 13, 2015
	
    #154 For missing properties, the key for that error in set to ""
    #159 Promote HHVM.
    #163 Enhancement: Use SVG badge for displaying Travis build status
    #164 Fix: Autoload test resources with Composer
    #165 Fix: Remove unused imports
    #166 Fix: Encourage to use phpunit as installed with Composer
    #167 Fix: Constraint::addError() does not return anything


1.4.2 - Jun 14, 2015

    #96 Update minimum PHP version to 5.3.2
    #105 Fix URI generic syntax delimit path components by slash ("/")
    #109 PHP < 5.4.0 compatibility for "--dump-schema"
    #125 Do not throw errors when using unknown/custom format
    #139 Remove composer.lock
    #150 [Constraints] make error messages better.
    #152 Fetching JSON Array from URI
    #153 Fix warning on file_get_contents
    #155 chore(travis): remove phpdoc ext and ignore output
    #156 [travis-ci] Add PHP 7 to build matrix
    #157 Scrutinizer-CI is not used


1.4.1 - Mar 27, 2015

    #132 Make the enum element comparison strict


1.4.0 - Mar 23, 2015

    #136 New constraints name - for PHP7

    This version breaks BC for anyone directly using the JsonSchema\Constraints\* classes.
    If you simply use this project via the JsonSchema\Validator class there are no issues.


1.3.7 - 24 Aug 2014

    #106 Fix UriRetriever::combineRelativePathWithBasePath() does not qualify UriResolverException
    #104 Constraint::check() uses wrong indefinite article in error message to express the type constraint
    #101 replace invalid filesystem dependent directory separator references
    #107 Added PHP 5.6 and HHVM to travis.yml
    #99 Use mb_strlen instead of strlen when mbstring extension is available.
    #98 Fix constraint errors getter
    #100 minor fix of BasicTypeTests
    #103 Fix required property validation


1.3.6 - 5 Mar 2014

    #92 added configurable maximum recursion depth
    #95 Fix a case of 2 invalid oneOf were considered valid
    #94 Add support for additionalProperties set to true


1.3.5 - 13 Dec 2013

    #86 - add specific php version 5.3.3 to travis build to validate issue #84
    #85 - remove abstract declaration of 'retrieve' which duplicated interface declaration


1.3.4 - 8 Dec 2013

    #54 - Fix UriRetriever test
    #60 - bugfix: JsonDecodingException message
    #73 - Ignore mime type on json-schema.org
    #72 - show path info in invalid-schema exception
    #74 - Cache schemas
    #75 - Fix for #62, #63, #64: automatically detect schema URL
    #78 - Add *Of properties to RefResolver
    #79 - fixed "empty array"-bug
    #83 - Validation with oneOf, allOf and anyOf only if property is set
    #68 - Add Sphinx Doc and PHPDocumentor to Travis-CI


1.3.3 - 22 Jul 2013 

1.3.2 - 10 Jun 2013

1.3.1 - 21 Feb 2013

1.3.0 - 18 Feb 2013

1.2.4 - 31 Jan 2013

1.2.3 - 29 Jan 2013

1.2.2 - 29 Nov 2012

1.2.1 - 18 Aug 2012

1.2.0 - 20 Jul 2012

1.1.1 - 6 May 2012

1.1.0 - 2 Jan 2012
